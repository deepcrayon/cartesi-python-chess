#!/usr/bin/python3

import chess.pgn
import logging

logging.getLogger("chess.pgn").setLevel(logging.CRITICAL)

pgn = open("/mnt/cartesi-python-chess/chess-game.pgn")
game = chess.pgn.read_game(pgn)

print(game)

game.errors

print(("Errors:"), game.errors)

